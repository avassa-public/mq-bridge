FROM --platform=$TARGETPLATFORM lukemathwalker/cargo-chef:latest AS chef
ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR app

FROM --platform=$TARGETPLATFORM chef as planner

COPY . .

ENV CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse

RUN cargo chef prepare --recipe-path recipe.json

# NOTE, copy from chef, which is "blank"
FROM --platform=$TARGETPLATFORM chef as builder

ENV RUSTFLAGS="-C target-feature=-crt-static"
ENV CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse

RUN apt-get update
RUN apt-get install -y cmake make
# RUN rustup component add rustfmt

COPY --from=planner /app/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json

COPY . .
RUN cargo build --release
RUN strip --strip-unneeded  target/release/avassa-mq-bridge


# RUN pwd
# RUN find . 

# Target image
FROM --platform=$TARGETPLATFORM gcr.io/distroless/cc-debian11 as applications

# RUN apk add libgcc 

COPY --from=builder /app/target/release/avassa-mq-bridge /usr/local/bin/avassa-mq-bridge

ENV SUPD https://api:4646
ENV RUST_LOG info
ENV PYTHONUNBUFFERED 1

# ENTRYPOINT [ "ls", "-als", "/usr/local/bin" ]
ENTRYPOINT [ "/usr/local/bin/avassa-mq-bridge" ]
