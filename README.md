# Avassa MQ Bridge
In bridge mode this application will act as a local cache for MQTT messages.
Volga is used as a local cache where MQTT messages are stored, as long as an
upstream connection is available, messages will be sent to the upstream MQTT
broker. When connection is lost, the messages are cached until the connection
is restored.

### Overview
![bridge](docs/bridge.png "Bridge Overview")

### Bridge Configuration
```
bridges:
    # Topic to use as cache, this topic is created by the bridge
  - volga-topic: mqtt-bridge
    # Local MQTT address
    listen-address: 0.0.0.0:11883
    # Optional local credentials
    listen-username: test-user
    listen-password: password

    # Upstream MQTT broker
    upstream-address: localhost:1883  
    # Optional upstream credentials
    upstream-username: test-user
    upstream-password: password
```
Also see the examples directory.
