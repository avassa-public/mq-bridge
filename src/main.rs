mod bridge;
mod config;
mod from_mqtt;
mod mqtt;
mod to_mqtt;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    tracing_subscriber::fmt::init();

    let config = config::load_config("config.yaml")?;

    let avassa_client = avassa_client::login_helper::login().await?;

    for exporter in config.to_mqtt {
        tokio::spawn(to_mqtt::start_export(avassa_client.clone(), exporter));
    }

    for importer in config.from_mqtt {
        tokio::spawn(from_mqtt::start_import(avassa_client.clone(), importer));
    }

    for bridge in config.bridges {
        tokio::spawn(bridge::run(avassa_client.clone(), bridge));
    }

    tokio::signal::ctrl_c().await?;

    tracing::info!("Shutting down");

    Ok(())
}
