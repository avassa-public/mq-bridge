pub async fn connect(cfg: &impl crate::config::MQTTInfo) -> paho_mqtt::AsyncClient {
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    loop {
        interval.tick().await;
        let mqtt_client = match paho_mqtt::AsyncClient::new(cfg.broker_address()) {
            Ok(c) => c,
            Err(e) => {
                tracing::error!("Failed to create mqtt client: {e}");
                continue;
            }
        };

        let opts = {
            let mut opts = paho_mqtt::connect_options::ConnectOptionsBuilder::new();

            cfg.mqtt_password().iter().for_each(|pwd| {
                tracing::debug!("Setting password");
                opts.password(pwd);
            });
            cfg.mqtt_username().iter().for_each(|user| {
                tracing::debug!("Setting password");
                opts.user_name(user);
            });

            opts.finalize()
        };

        let connect = match mqtt_client.connect(opts).await {
            Ok(c) => c,
            Err(e) => {
                tracing::error!("Failed to connect to {}, error: {e}", cfg.broker_address());
                continue;
            }
        };

        if connect.reason_code().is_err() {
                tracing::error!("Failed to connect to {}, error: {}", cfg.broker_address(), connect.reason_code());
                continue;
        }
        return mqtt_client;
    }
}
