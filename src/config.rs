use anyhow::Context;

pub trait MQTTInfo {
    fn broker_address(&self) -> &str;
    fn mqtt_username(&self) -> Option<String>;
    fn mqtt_password(&self) -> Option<String>;
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum Format {
    Base64,
    Json,
    String,
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct MQTTCommon {
    pub volga_topic: String,
    pub mqtt_broker: String,
    pub mqtt_topic: String,
    pub mqtt_username: Option<String>,
    pub mqtt_password: Option<String>,
    pub volga_format: Format,
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct ToMQTT {
    #[serde(flatten)]
    pub common: MQTTCommon,
    pub mqtt_qos: i32,
}

impl MQTTInfo for ToMQTT {
    fn broker_address(&self) -> &str {
        &self.common.mqtt_broker
    }

    fn mqtt_username(&self) -> Option<String> {
        self.common.mqtt_username.clone()
    }

    fn mqtt_password(&self) -> Option<String> {
        self.common.mqtt_password.clone()
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct FromMQTT {
    #[serde(flatten)]
    pub common: MQTTCommon,
    pub mqtt_qos: i32,
}

impl MQTTInfo for FromMQTT {
    fn broker_address(&self) -> &str {
        &self.common.mqtt_broker
    }

    fn mqtt_username(&self) -> Option<String> {
        self.common.mqtt_username.clone()
    }

    fn mqtt_password(&self) -> Option<String> {
        self.common.mqtt_password.clone()
    }
}

#[derive(Clone, Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct MQTTBridge {
    pub listen_address: String,
    pub listen_username: Option<String>,
    pub listen_password: Option<String>,
    pub volga_topic: String,
    upstream_address: String,
    upstream_username: Option<String>,
    upstream_password: Option<String>,
}

impl MQTTBridge {
    pub fn local_credentials(&self) -> Option<(&str, &str)> {
        self.listen_username
            .as_ref()
            .map(|username| {
                self.listen_password
                    .as_ref()
                    .map(|password| (username.as_str(), password.as_str()))
            })
            .flatten()
    }
}

impl MQTTInfo for MQTTBridge {
    fn broker_address(&self) -> &str {
        &self.upstream_address
    }

    fn mqtt_username(&self) -> Option<String> {
        self.upstream_username.clone()
    }

    fn mqtt_password(&self) -> Option<String> {
        self.upstream_password.clone()
    }
}

#[derive(Debug, serde::Deserialize)]
#[serde(rename_all = "kebab-case")]
pub struct Config {
    #[serde(default)]
    pub to_mqtt: Vec<ToMQTT>,
    #[serde(default)]
    pub from_mqtt: Vec<FromMQTT>,
    #[serde(default)]
    pub bridges: Vec<MQTTBridge>,
}

pub fn load_config(p: impl AsRef<std::path::Path>) -> anyhow::Result<Config> {
    let data = std::fs::read_to_string(p.as_ref())
        .context(format!("Load {}", p.as_ref().to_string_lossy()))?;
    let config = serde_yaml::from_str(&data)?;
    Ok(config)
}
