use base64::prelude::{Engine as _, BASE64_STANDARD_NO_PAD};
use futures_util::StreamExt;

#[tracing::instrument(skip(avassa_client, cfg))]
pub async fn start_import(mut avassa_client: avassa_client::Client, cfg: crate::config::FromMQTT) {
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    // Loop until we can connect the first time.
    let mut mqtt_client = crate::mqtt::connect(&cfg).await;

    loop {
        if let Err(e) = import_loop(&mut avassa_client, &mut mqtt_client, &cfg).await {
            tracing::error!(%e);
        }
        interval.tick().await;
    }
}

async fn import_loop(
    avassa_client: &mut avassa_client::Client,
    mqtt_client: &mut paho_mqtt::AsyncClient,
    cfg: &crate::config::FromMQTT,
) -> anyhow::Result<()> {
    if !mqtt_client.is_connected() {
        let conn = mqtt_client.reconnect().await;
        match conn {
            Ok(conn) if conn.connect_response().is_some() => (),
            _ => {
                return Err(anyhow::anyhow!("Failed to reconnect to server"));
            }
            
        }
    }
    let volga_format = match cfg.common.volga_format {
        crate::config::Format::Json => avassa_client::volga::Format::JSON,
        crate::config::Format::Base64 => avassa_client::volga::Format::String,
        crate::config::Format::String => avassa_client::volga::Format::String,
    };
    let mut volga = avassa_client
        .volga_open_producer(
            "avassa-mqtt",
            &cfg.common.volga_topic,
            avassa_client::volga::OnNoExists::Create(avassa_client::volga::CreateOptions {
                format: volga_format,
                ..Default::default()
            }),
        )
        .await?;

    tracing::info!("Opened volga consumer");


    let mqtt_sub = mqtt_client
        .subscribe(&cfg.common.mqtt_topic, cfg.mqtt_qos)
        .await?;

    if mqtt_sub.reason_code().is_err() {
        return Err(anyhow::anyhow!(
            "MQTT subscribe failed: {}",
            mqtt_sub.reason_code()
        ));
    }

    let mut mqtt_stream = mqtt_client.get_stream(1);

    while let Some(Some(msg)) = mqtt_stream.next().await {
        tracing::info!(?msg);

        let payload = match cfg.common.volga_format {
            crate::config::Format::Base64 => BASE64_STANDARD_NO_PAD.encode(msg.payload()),
            crate::config::Format::Json => {
                let json: serde_json::Value = serde_json::from_slice(msg.payload())?;
                serde_json::to_string(&json)?
            }
            crate::config::Format::String => String::from_utf8(msg.payload().to_vec())?,
        };

        volga.produce(&payload).await?;
    }
    Ok(())
}
