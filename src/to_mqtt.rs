use base64::prelude::{Engine as _, BASE64_STANDARD_NO_PAD};
#[tracing::instrument(skip(avassa_client, cfg))]
pub async fn start_export(mut avassa_client: avassa_client::Client, cfg: crate::config::ToMQTT) {
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    let mut mqtt_client = crate::mqtt::connect(&cfg).await;
    loop {
        interval.tick().await;
        if let Err(e) = export_loop(&mut avassa_client, &mut mqtt_client, &cfg).await {
            tracing::error!("{e}");
        }
    }
}

async fn export_loop(
    avassa_client: &mut avassa_client::Client,
    mqtt_client: &mut paho_mqtt::AsyncClient,
    cfg: &crate::config::ToMQTT,
) -> anyhow::Result<()> {
    if !mqtt_client.is_connected() {
        let conn = mqtt_client.reconnect().await;
        match conn {
            Ok(conn) if conn.connect_response().is_some() => (),
            _ => {
                return Err(anyhow::anyhow!("Failed to reconnect to server"));
            }
            
        }
    }

    let mut volga = avassa_client
        .volga_open_consumer(
            "avassa-mqtt",
            &cfg.common.volga_topic,
            avassa_client::volga::consumer::Options {
                position: avassa_client::volga::consumer::Position::Unread,
                ..Default::default()
            },
        )
        .await?;

    tracing::info!("Opened volga consumer");


    loop {
        let volga_msg = match &cfg.common.volga_format {
            crate::config::Format::Base64 => {
                let volga_msg = volga.consume::<String>().await?;

                BASE64_STANDARD_NO_PAD.decode(&volga_msg.payload)?
            }
            crate::config::Format::String => {
                let volga_msg = volga.consume::<String>().await?;
                volga_msg.payload.as_bytes().to_vec()
            }
            crate::config::Format::Json => {
                let volga_msg = volga.consume::<serde_json::Value>().await?;
                serde_json::to_vec(&volga_msg)?
            }
        };

        tracing::info!(?volga_msg);

        let mqtt_msg = paho_mqtt::Message::new(&cfg.common.mqtt_topic, volga_msg, cfg.mqtt_qos);

        mqtt_client.publish(mqtt_msg).await?;
    }
}
