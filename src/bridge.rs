use tokio::io::{AsyncReadExt, AsyncWriteExt};

// Need to derive serde for this type
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, serde::Serialize, serde::Deserialize)]
#[allow(clippy::enum_variant_names)]
enum QoS {
    AtMostOnce = 0,
    AtLeastOnce = 1,
    ExactlyOnce = 2,
}

impl From<mqttbytes::QoS> for QoS {
    fn from(value: mqttbytes::QoS) -> Self {
        match value {
            mqttbytes::QoS::AtMostOnce => Self::AtMostOnce,
            mqttbytes::QoS::AtLeastOnce => Self::AtLeastOnce,
            mqttbytes::QoS::ExactlyOnce => Self::ExactlyOnce,
        }
    }
}

impl From<QoS> for mqttbytes::QoS {
    fn from(value: QoS) -> Self {
        match value {
            QoS::AtMostOnce => Self::AtMostOnce,
            QoS::AtLeastOnce => Self::AtLeastOnce,
            QoS::ExactlyOnce => Self::ExactlyOnce,
        }
    }
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
struct MQTTMessage {
    topic: String,
    qos: QoS,
    retain: bool,
    payload: bytes::Bytes,
}

impl From<MQTTMessage> for paho_mqtt::Message {
    fn from(value: MQTTMessage) -> Self {
        if value.retain {
            Self::new_retained(value.topic, value.payload, value.qos as _)
        } else {
            Self::new(value.topic, value.payload, value.qos as _)
        }
    }
}

impl std::fmt::Debug for MQTTMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("MQTTMessage")
            .field("topic", &self.topic)
            .field("qos", &self.qos)
            .field("retain", &self.retain)
            .finish()
    }
}

impl From<mqttbytes::v4::Publish> for MQTTMessage {
    fn from(value: mqttbytes::v4::Publish) -> Self {
        Self {
            topic: value.topic,
            qos: value.qos.into(),
            retain: value.retain,
            payload: value.payload,
        }
    }
}

type MQTTSender = tokio::sync::mpsc::Sender<MQTTMessage>;
type MQTTReceiver = tokio::sync::mpsc::Receiver<MQTTMessage>;

#[tracing::instrument(skip(avassa_client, cfg))]
pub async fn run(avassa_client: avassa_client::Client, cfg: crate::config::MQTTBridge) {
    tracing::info!("Listening to {}", cfg.listen_address);

    let sa: Vec<_> = tokio::net::lookup_host(cfg.listen_address.clone())
        .await
        .unwrap()
        .collect();
    let listener = tokio::net::TcpListener::bind(&sa[..]).await.unwrap();

    let (mqtt_tx, mqtt_rx) = tokio::sync::mpsc::channel(10);

    tokio::spawn(to_volga(avassa_client.clone(), cfg.clone(), mqtt_rx));
    tokio::spawn(from_volga(avassa_client.clone(), cfg.clone()));

    loop {
        tokio::select! {
            client = listener.accept() => match client {
                Ok((sock, addr)) => {
                    tracing::info!("Client connection from {addr}");
                    tokio::spawn(handle_client(cfg.clone(), sock, addr, mqtt_tx.clone()));
                }
                Err(e) => {
                    tracing::error!("accept: {e}");
                }
            },
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
enum ClientState {
    New,
    ConnAckSent,
}

#[derive(Debug)]
struct Client {
    connect: mqttbytes::v5::Connect,
    outgoing: bytes::BytesMut,
    state: ClientState,
    mqtt_tx: MQTTSender,
    cfg: crate::config::MQTTBridge,
}

#[derive(PartialEq)]
enum Next {
    Continue,
    Disconnect,
}

impl Client {
    fn new(cfg: crate::config::MQTTBridge, mqtt_tx: MQTTSender) -> Self {
        let outgoing = bytes::BytesMut::with_capacity(1024);
        Self {
            cfg,
            connect: mqttbytes::v5::Connect {
                protocol: mqttbytes::Protocol::V5,
                keep_alive: 0,
                client_id: String::new(),
                clean_session: true,
                last_will: None,
                login: None,
                properties: None,
            },
            state: ClientState::New,
            outgoing,
            mqtt_tx,
        }
    }

    async fn on_packet(&mut self, buf: &mut bytes::BytesMut) -> anyhow::Result<Next> {
        if self.connect.protocol == mqttbytes::Protocol::V5 {
            self.packet_v5(buf).await
        } else {
            self.packet_v4(buf).await
        }
    }

    async fn packet_v5(&mut self, buf: &mut bytes::BytesMut) -> anyhow::Result<Next> {
        let len = buf.len();
        let msg = mqttbytes::v5::read(buf, len)?;
        tracing::info!(?msg);
        match msg {
            mqttbytes::v5::Packet::Connect(c) if self.state == ClientState::New => {
                tracing::info!("Connect received");
                self.connect = c;
                self.state = ClientState::ConnAckSent;
                tracing::info!("Protocol: {:?}", self.connect.protocol);

                match (self.cfg.local_credentials(), self.connect.login.as_ref()) {
                    // match username/pwd
                    (Some((username, password)), Some(l))
                        if l.username == username && l.password == password =>
                    {
                        ()
                    }
                    // No credentials required and none sent
                    (None, None) => (),
                    _ => {
                        tracing::error!("Client failed to authenticate");
                        let packet = mqttbytes::v4::ConnAck {
                            code: mqttbytes::v4::ConnectReturnCode::BadUserNamePassword,
                            session_present: false, 
                        };

                        let _ = packet.write(&mut self.outgoing)?;
                        return Ok(Next::Disconnect);
                    }
                }

                if self.connect.protocol == mqttbytes::Protocol::V5 {
                    let packet = mqttbytes::v5::ConnAck {
                        session_present: false,
                        code: mqttbytes::v5::ConnectReturnCode::Success,
                        properties: None,
                    };
                    let _ = packet.write(&mut self.outgoing)?;
                } else {
                    let packet = mqttbytes::v4::ConnAck {
                        session_present: false,
                        code: mqttbytes::v4::ConnectReturnCode::Success,
                    };
                    let _ = packet.write(&mut self.outgoing)?;
                }
            }
            _ => todo!(),
        }
        Ok(Next::Continue)
    }

    async fn packet_v4(&mut self, buf: &mut bytes::BytesMut) -> anyhow::Result<Next> {
        let len = buf.len();
        let msg = mqttbytes::v4::read(buf, len)?;
        tracing::info!(?msg);
        match msg {
            mqttbytes::v4::Packet::Publish(p) => {
                tracing::info!("v4 publish");
                let resp = mqttbytes::v4::PubAck { pkid: p.pkid };
                // We need to handle QoS here
                self.mqtt_tx.send(p.into()).await?;
                resp.write(&mut self.outgoing)?;
            }
            mqttbytes::v4::Packet::Disconnect => {
                //
                return Ok(Next::Disconnect);
            }
            p => {
                return Err(anyhow::anyhow!("Unhandled v4 packet {p:?}"));
            }
        }
        Ok(Next::Continue)
    }
}

async fn handle_client(
    cfg: crate::config::MQTTBridge,
    mut sock: tokio::net::TcpStream,
    addr: std::net::SocketAddr,
    mqtt_tx: MQTTSender,
) {
    let mut incoming = bytes::BytesMut::with_capacity(2028);
    let mut client = Client::new(cfg, mqtt_tx);
    loop {
        incoming.reserve(1024);
        let cur_len = incoming.len();
        incoming.resize(incoming.capacity(), 0);
        match sock.read(&mut incoming[cur_len..]).await {
            Ok(n) if n > 0 => {
                incoming.truncate(cur_len + n);
                // tracing::info!(
                //     "len: {} capa: {} n: {n}",
                //     incoming.len(),
                //     incoming.capacity()
                // );

                match client.on_packet(&mut incoming).await {
                    Ok(next) => {
                        if !client.outgoing.is_empty() {
                            match sock.write_all(&client.outgoing).await {
                                Ok(_) => (),
                                Err(e) => {
                                    tracing::error!("socket write error: {e}");
                                    return;
                                }
                            }
                            client.outgoing.truncate(0);
                        }

                        if next == Next::Disconnect {
                            tracing::info!("Disconnected");
                            return;
                        }
                    }
                    Err(e) => {
                        tracing::error!("Client on_packet error: {e}");
                        return;
                    }
                }
                tracing::info!(
                    "len: {} capa: {} n: {n}",
                    incoming.len(),
                    incoming.capacity()
                );
            }
            Ok(_) => {
                tracing::info!("no data");
                return;
            }
            Err(e) => {
                tracing::error!("accept: {e} from {addr}");
                return;
            }
        }
    }
}

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
struct VolgaMsg {
    // Base64 encoded
    msg: String,
    topic: String,
    qos: usize,
}

async fn to_volga(
    avassa_client: avassa_client::Client,
    cfg: crate::config::MQTTBridge,
    mut mqtt_rx: MQTTReceiver,
) {
    let opts = avassa_client::volga::OnNoExists::Create(avassa_client::volga::CreateOptions {
        replication_factor: 3,
        format: avassa_client::volga::Format::JSON,
        ..Default::default()
    });
    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    'volga_connect: loop {
        interval.tick().await;
        tracing::info!("Open producer");
        match avassa_client
            .volga_open_producer("mqtt-bridge", &cfg.volga_topic, opts)
            .await
        {
            Ok(mut volga) => {
                tracing::info!("Waiting for messages");
                loop {
                    match mqtt_rx.recv().await {
                        None => {
                            tracing::info!("mqtt_rx channel closed, shutting down");
                            return;
                        }
                        Some(msg) => {
                            tracing::info!("Store {msg:?} on volga");
                            let msg = serde_json::to_string(&msg).unwrap();
                            if let Err(e) = volga.produce(&msg).await {
                                tracing::error!("volga produce: {e}");
                                continue 'volga_connect;
                            }
                        }
                    }
                }
            }
            Err(e) => {
                tracing::error!("open producer: {e}");
            }
        }
    }
}

async fn from_volga(avassa_client: avassa_client::Client, cfg: crate::config::MQTTBridge) {
    let opts = avassa_client::volga::consumer::Options {
        position: avassa_client::volga::consumer::Position::Unread,
        on_no_exists: avassa_client::volga::OnNoExists::Create(
            avassa_client::volga::CreateOptions {
                replication_factor: 3,
                format: avassa_client::volga::Format::JSON,
                ..Default::default()
            },
        ),
        ..Default::default()
    };

    let mut interval = tokio::time::interval(std::time::Duration::from_secs(30));
    interval.set_missed_tick_behavior(tokio::time::MissedTickBehavior::Skip);

    let mqtt_client = crate::mqtt::connect(&cfg).await;
    let mut volga = None;
    let mut cached_message: Option<avassa_client::volga::consumer::MessageMetadata<MQTTMessage>> =
        None;

    loop {
        interval.tick().await;

        if !mqtt_client.is_connected() {
            let conn = mqtt_client.reconnect().await;
            if let Err(e) = conn {
                tracing::error!("mqtt reconnect failed: {e}");
                continue;
            }
        }

        if volga.is_none() {
            tracing::info!("Open producer");
            match avassa_client
                .volga_open_consumer("mqtt-bridge", &cfg.volga_topic, opts)
                .await
            {
                Ok(v) => volga = Some(v),
                Err(e) => {
                    tracing::error!("open consumer error: {e}");
                    continue;
                }
            }
        }

        // Safe to unwrap here, we know volga is_some
        let consumer = volga.as_mut().unwrap();
        'msg_loop: loop {
            let msg = if let Some(msg) = cached_message.take() {
                msg
            } else {
                match consumer.consume::<MQTTMessage>().await {
                    Ok(msg) => {
                        tracing::info!("volga -> mqtt");
                        msg
                    }
                    Err(e) => {
                        tracing::error!("volga consume {e}");
                        // Force reconnection of volga
                        volga = None;
                        break 'msg_loop;
                    }
                }
            };

            let mqtt_msg: paho_mqtt::Message = msg.payload.clone().into();
            if let Err(e) = mqtt_client.publish(mqtt_msg).await {
                cached_message = Some(msg);
                tracing::error!("mqtt publish: {e}");
                break 'msg_loop;
            }

            // Ack the message after successful send
            consumer.ack(msg.seqno).await.unwrap();
        }
    }
}
